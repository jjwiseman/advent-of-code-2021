use io::BufRead;
// use itertools::Itertools;
use std::io;

pub fn read_numbers() -> Vec<i32> {
    let stdin = io::stdin();
    let mut numbers = Vec::new();
    for line in stdin.lock().lines() {
        let value = line.unwrap().parse::<i32>().unwrap();
        numbers.push(value);
    }
    numbers
}
